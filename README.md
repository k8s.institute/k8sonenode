# k8sonenode cluster by [k8s.institute](https://k8s.institute)

## Build one node kubernetes environment for local development with Vagrant and VirtualBox 
#
#
> Prequisites : You will need [Vagrant](https://www.vagrantup.com/)  and [VirtualBox](https://download.virtualbox.org/virtualbox/6.0.10/)  installed on your system to take advantage of quick install. We have configured everything in Vagrantfile for kubernetes installation using attached shell script. You just need to run `vagrant up`. Follow these steps.
---
> Create a new directory and then clone the repository inside that directory to isolate your work.
---
```
mkdir k8slocal
cd k8slocal
```
> Now clone this repository on your system or just download Vagrantfile and [```k8s_institute_ubuntu_script.sh```] 
```
git@gitlab.com:k8s.institute/k8sonenode.git
cd k8sonenode
vagrant up
```
#### This will take few minutes to install everything you need to setup local kubernetes cluster.
---
#### Once installation is successfull, log in to virtual machine and execute kubectl to watch status of all resources. If you see some containers are in `ContainerCreate` or `Init` state then wait for few minutes until they all are in `Running` state

```
vagrant ssh
```
```
kubectl get all -n kube-system
```
#### OR you can also run kubectl with super user as follow.
```
sudo -i
kubectl get all -n kube-system
```
#### We can also install kubectl client utility on our host computer and access kubernetes cluster through api after copying the config locally. Steps to follow :

```
mkdir ~/.kube #Make sure you create .kube directory in your home directory.
cd ~/.kune
scp vagrant@10.10.10.87:/home/vagrant/.kube/config .
```
#### Lets check the version using following command 
#### The first command will only return client version.
#### The second command will return client and server versions.
```
kubectl version --client
OR
kubectl version
```
#### ---------------------------------------------------
```
kubectl help
kubectl get all -n kube-system
```
#### you should see output similar to:
![kubectl](k8s.institute.kubectl_get_all.png)

#### To view all namespaces after installation.
```
kubectl get namespaces
OR
kubectl get ns
```
#### To create a pod with imperative command. Following command will create pod in `default` namespace if you have not changed your current namespace after installation..
```
kubectl run nginx --image=nginx
```
#### To get list of pods use following command.
```
kubectl get pods
OR
kubectl get pods -o wide
```
#### To get list of pods from all namespaces use following command.

```
kubectl get pods --all-namespaces
OR
kubectl get pods --all-namespaces -o wide
```
---

> More details can be found at : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

###### Kubernetes k8s DevOps microservice Deployment Pod Node Service ReplicaSet rs no po deploy svc DaemonSet ConfigMap cm Secret env environment volume pv pvc PhysicalVolume PhysicalVolumeClaim

*http://www.ninit.tech*
*https://www.k8s.institute*
*https://k8s.institute*
## Our instructors are ```Certified Kubernetes Administrator``` and ```Certified Kubernetes Application Developer```
